﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class EndCozinha : MonoBehaviour
{
    public Text endscore, endmoedas, endhs;

    // Start is called before the first frame update
    void Start()
    {
        endscore.text = "Pontuação: " + Static.score;
        endmoedas.text = "Moedas: " + Static.moedas;
        endhs.text = "Maior Pontuação: " + Static.highscore;
    }

    public void JogarDeNovo()
    {
        SceneManager.LoadScene("MiniGame Cozinha");
    }

    public void ResetPlaytest()
    {
        Static.moedas = 0;
        Static.highscore = 0;
    }
}
