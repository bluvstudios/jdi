﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LojaScript : MonoBehaviour
{
    public GameObject panelloja, pRoupa, pComida;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void OpenLoja()
    {
        panelloja.SetActive(true);

        if (SwipeBackground.estado == "cozinha")
        {
            pRoupa.SetActive(false);
        }
        else pComida.SetActive(false);
    }
    public void CloseLoja()
    {
        panelloja.SetActive(false);
    }

    public void ButtonRoupa()
    {
        pRoupa.SetActive(true);
        pComida.SetActive(false);
    }
    public void ButtonComida()
    {
        pRoupa.SetActive(false);
        pComida.SetActive(true);
    }
}
