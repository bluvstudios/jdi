﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TouchScript.Gestures;
using UnityEngine.UI;

public class Drops : MonoBehaviour
{
    public string tipo;
    TapGesture tap;


   public static int pontosContador = 0;
    public static int vida = 3;

    public static bool perdeu;




    void Awake()
    {
        tap = GetComponent<TapGesture>();

    }


    private void OnEnable()
    {
        tap.Tapped += Processa;
    }

    private void OnDisable()
    {
        tap.Tapped -= Processa;

    }


    private void Processa(object sender, System.EventArgs e)
    {
        if(objetivo.cor == gameObject.tag)
        {
            Destroy(gameObject);
            destruiu = true;

            pontosContador++;

            // outravariavel = true;

        }
    }

    void Update()
    {
        destroy();

        if(vida == 0)
        {
            perdeu = true;
        }
    }

    public static bool destruiu;

    void destroy()
    {
        if(transform.position.y <= -10)
        {


            destruiu = true;
            objetivo.check = true;
            Destroy(gameObject);

            if(objetivo.cor == gameObject.tag)
            {
                vida--;
            }

        }
    }
}
