﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

public class DataManager : MonoBehaviour {

    void Start () {

        string json = File.ReadAllText (Application.dataPath + "/Zelo/Scripts/data.json");
        Debug.Log(json);
        comidaData getData = JsonUtility.FromJson<comidaData> (json);
        Debug.Log ("A comida é " + getData.key);

    }

    [System.Serializable]
    private class comidaData {
        public string key;
        public string preço;
    }

}