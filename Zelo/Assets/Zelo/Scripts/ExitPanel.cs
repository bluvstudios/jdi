﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ExitPanel : MonoBehaviour
{
    public GameObject Panel, settingsButton, credits, creditsExit, buddyCollision;

    public void PanelExit()
    {
        if (Panel != null)
        {

            Panel.SetActive(false);
            settingsButton.SetActive(true);
            buddyCollision.GetComponent<BoxCollider2D>().enabled = true;
        }
    }

    public void Credits()
    {

        buddyCollision.GetComponent<BoxCollider2D>().enabled = false;
        credits.SetActive(true);
       // Panel.SetActive(false);


    }

    public void CreditsExit()
    {

        buddyCollision.GetComponent<BoxCollider2D>().enabled = false;
        credits.SetActive(false);
        Panel.SetActive(true);

    }
}