﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using UnityEngine.UI;


public class Barras : MonoBehaviour
{
    int hungerValue = 100;
    int glicoseValue = 100;
    int hpValue = 100;
    bool lockUpdateHungerIn, lockUpdateGlicoseIn, ifInsulina;
    GameObject[] h = new GameObject[10];
    GameObject[] v = new GameObject[10];
    GameObject g;
    public float teste;
    int insulinaT;
    float anterior, now, difference;
    public Text gtext, moedatext;

    // Start is called before the first frame update
    void Start()
    {
        int getcoins = PlayerPrefs.GetInt("Moedas", 0);
        if(getcoins != -1)
        {
            Static.moedas = getcoins;
        }
        else
        {
            PlayerPrefs.SetInt("Moedas", 0);
            Static.moedas = 0;
        }

        g = GameObject.Find("g0");
        //g.transform.position = new Vector3(teste, g.transform.position.y, g.transform.position.z);

        for (int i = 0; i < 10; i++)
        {
            h[i] = GameObject.Find("h" + i);
        }
        for (int i = 0; i < 10; i++)
        {
            v[i] = GameObject.Find("v" + i);
        }

        anterior = gameObject.transform.position.x;

        #region StartTime
        DateTime nowT = System.DateTime.Now;
        long conversionT = Convert.ToInt64(PlayerPrefs.GetString("leavingT"));
        DateTime lastT = DateTime.FromBinary(conversionT);
        TimeSpan differenceT = nowT.Subtract(lastT);
        double differenceTd = differenceT.TotalSeconds;
        int differenceTint = Mathf.RoundToInt((float)differenceTd);

        #endregion

        #region StartGlicose

        int insulinaT1 = insulinaT - differenceTint;
        if (insulinaT1 <= 0)
        {
            ifInsulina = false;
            int glicoseold, glicosenew;
            //Glicemia Baixa- 70, 50, 45, 30
            //Glicemia Alta -160, 200, 250, 300

            glicoseold = PlayerPrefs.GetInt("leavingGlicoseValue");
            glicosenew = glicoseold - (insulinaT / 4);


            if (insulinaT == 0)
            {
                if (glicoseold >= 160)
                {
                    int glic_graph = glicoseold - 160;
                    hpValue = hpValue - (glic_graph * differenceTint / 40);
                }
                else
                {
                    int glic_graph = glicoseold - 100;
                    hpValue = hpValue + ((60 - glic_graph) * differenceTint / 20);
                }
            }
            else
            {
                if(glicoseold >= 160)
                {
                    if(glicosenew >= 160)
                    {
                        int glic_graph = glicosenew + ((glicosenew - glicoseold)/2) - 160;
                        hpValue = hpValue - (glic_graph * differenceTint / 720);
                    }
                    else
                    {
                        int seg = (glicoseold - 160) * 4;
                        int glic_up = glicoseold - 160;
                        hpValue = hpValue - (glic_up * seg / 40);
                        int glic_down = glicosenew - 100;
                        hpValue = hpValue + ((60 - glic_down) * (differenceTint - seg) / 20);
                    }
                }
                else
                {
                    int glic_graph = glicosenew + ((glicosenew - glicoseold) / 2) - 100;
                    hpValue = hpValue + ((60 - glic_graph) * differenceTint / 20);
                }
            }


            /*if (insulinaT == 0)
            {
                if(glicoseold >= 160)
                {
                    int glic_graph = glicoseold - 160;
                    hpValue = hpValue - (glic_graph * differenceTint / 720);
                }
                else
                {
                    int glic_graph = glicoseold - 100;
                    hpValue = hpValue + ((60 - glic_graph) * differenceTint / 360);
                }
            }
            else
            {
                if(glicosenew <= 100)
                {
                    int overboard = -(glicosenew + 100);
                    if(glicoseold > 160)
                    {
                        hpValue = hpValue + (60 / 10);

                    }
                }
                else
                {

                }
            }
            */
            glicoseValue = PlayerPrefs.GetInt("leavingGlicoseValue") - (insulinaT / 4);
            if (glicoseValue < 100) glicoseValue = 100;
            insulinaT = 0;
        }
        else
        {

            int glicoseold, glicosenew;
            //Glicemia Baixa- 70, 50, 45, 30
            //Glicemia Alta -160, 200, 250, 300

            glicoseold = PlayerPrefs.GetInt("leavingGlicoseValue");
            glicosenew = glicoseold - (insulinaT / 4);

            if (glicoseold >= 160)
            {
                if (glicosenew >= 160)
                {
                    int glic_graph = glicosenew + ((glicosenew - glicoseold) / 2) - 160;
                    hpValue = hpValue - (glic_graph * differenceTint / 40);
                }
                else
                {
                    int seg = (glicoseold - 160) * 4;
                    int glic_up = glicoseold - 160;
                    hpValue = hpValue - (glic_up * seg / 40);
                    int glic_down = glicosenew - 100;
                    hpValue = hpValue + ((60 - glic_down) * (differenceTint - seg) / 20);
                }
            }
            else
            {
                int glic_graph = glicosenew + ((glicosenew - glicoseold) / 2) - 100;
                hpValue = hpValue + ((60 - glic_graph) * differenceTint / 20);
            }

            glicoseValue = PlayerPrefs.GetInt("leavingGlicoseValue") - (differenceTint / 4);
            if (glicoseValue < 100) glicoseValue = 100;
            insulinaT = insulinaT1;
        }

        UpdateGlicoseBar();



        #endregion

        hungerValue = PlayerPrefs.GetInt("leavingHungerValue") - (differenceTint/11);
        if (hungerValue < 0) hungerValue = 0;
        UpdateHungerBar();

        UpdateMoedas();

        if(hpValue > 100)
        {
            hpValue = 100;
        }
    }

    // Update is called once per frame
    void Update()
    {
        now = gameObject.transform.position.x;
        difference = now - anterior;

        //UpdateGlicoseBar();

        //hungerValue++; Debug.Log(hungerValue);
        if (lockUpdateHungerIn == false)
        {
            lockUpdateHungerIn = true;
            Invoke("UpdateHungerIn", 11f);
            Debug.Log(hungerValue);
        }
        if (lockUpdateGlicoseIn == false)
        {
            lockUpdateGlicoseIn = true;
            Invoke("UpdateGlicoseIn", 4f);
        }
    }

    public void Alimentar()
    {
        hungerValue = hungerValue + 10;
        if (hungerValue > 100) hungerValue = 100;
        glicoseValue = glicoseValue + 15;
        if (glicoseValue > 250) glicoseValue = 250;
        UpdateGlicoseBar();
        UpdateHungerBar();
    }

    void UpdateHungerBar()
    {
        Debug.Log(hungerValue);
        int decHunger = hungerValue/10;
        for (int i = 0; i<10; i++)
        {
            if (i <= decHunger)
            {
                h[i].SetActive(true);
            }
            if (i > decHunger)
            {
                h[i].SetActive(false);
            }
        }
        UpdateHealthBar();
    }
    void UpdateHealthBar()
    {
        Debug.Log(hpValue);
        int decHP = hpValue / 10;
        for (int i = 0; i < 10; i++)
        {
            if (i <= decHP)
            {
                v[i].SetActive(true);
            }
            if (i > decHP)
            {
                v[i].SetActive(false);
            }
        }
    }

    void UpdateMoedas()
    {
        moedatext.text = ("" + Static.moedas);
    }

    void UpdateGlicoseBar()
    {
        gtext.text = ("" + glicoseValue);
        UpdateHealthBar();
    }

    void UpdateHungerIn()
    {
        Debug.Log("roda");
        hungerValue = hungerValue - 1 ;
        lockUpdateHungerIn = false;

        if (hungerValue >= 70)
        {
            int hung_graph = hungerValue - 70;
            hpValue = hpValue + ((30 - hung_graph) / 5);
        }
        else if (hungerValue <= 30)
        {
            int hung_graph = hungerValue - 30;
            hpValue = hpValue - (hung_graph / 10);
        }

        UpdateHungerBar();
        SavePrefs();
    }
    void UpdateGlicoseIn()
    {
        Debug.Log("roda");
        if (ifInsulina == true)
        {
            insulinaT = insulinaT - 4 ;

            if (glicoseValue > 100)
            {
                glicoseValue = glicoseValue - 1;
            }
            lockUpdateGlicoseIn = false;

            if (insulinaT <= 0)
            {
                ifInsulina = false;
                insulinaT = 0;
            }
        }

        if (glicoseValue >= 160)
        {
            int glic_graph = glicoseValue - 160;
            hpValue = hpValue - (glic_graph / 10);
        }
        else
        {
            int glic_graph = glicoseValue - 100;
            hpValue = hpValue + ((60 - glic_graph) / 5);
        }

        UpdateGlicoseBar();

        SavePrefs();
    }

    public void ShotInsulina()
    {
        ifInsulina = true;
        insulinaT = 86400;
    }

    public void SavePrefs()
    {
        PlayerPrefs.SetString("leavingT", System.DateTime.Now.ToBinary().ToString());
        PlayerPrefs.SetInt("leavingHungerValue", hungerValue);
        PlayerPrefs.SetInt("leavingGlicoseValue", glicoseValue);
    }

    void OnApplicationQuit()
    {
        SavePrefs();
    }
}
