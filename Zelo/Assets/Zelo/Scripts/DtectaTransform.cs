﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TouchScript.Gestures.TransformGestures;
public class DtectaTransform : MonoBehaviour
{
    //GameObject objTranslation;
    TransformGesture gesto;

    public bool Horizontal;
    public float limiteEsquerdo,LimiteDireito;

    public bool Vertical;
    public float LimiteCima, LimiteBaixo;


    void Awake()
    {
       // objTranslation = GameObject.Find("Objeto");
        gesto = GetComponent<TransformGesture>();
    }

    void OnEnable()
    {
        gesto.Transformed += Processa;
    }

    void OnDisable()
    {
        gesto.Transformed -= Processa;
    }
    private void Processa(object sender, System.EventArgs e)
    {

        if (Horizontal)
        {

            if (transform.position.x + gesto.DeltaPosition.x >= LimiteDireito)
                transform.position = new Vector2(LimiteDireito, transform.position.y);
            if (transform.position.x + gesto.DeltaPosition.x <= limiteEsquerdo)
                transform.position = new Vector2(limiteEsquerdo, transform.position.y);


            if (transform.position.x + gesto.DeltaPosition.x > limiteEsquerdo && transform.position.x + gesto.DeltaPosition.x < LimiteDireito)
                transform.Translate(gesto.DeltaPosition.x, 0, 0);
        }

        if (Vertical)
        {
            if (transform.position.y + gesto.DeltaPosition.y >= LimiteCima)
                transform.position = new Vector2(transform.position.x, LimiteCima);
            if (transform.position.y + gesto.DeltaPosition.y <= LimiteBaixo)
                transform.position = new Vector2(transform.position.x, LimiteBaixo);

            if (transform.position.y + gesto.DeltaPosition.y > LimiteBaixo && transform.position.y + gesto.DeltaPosition.y < LimiteCima)
                transform.Translate(0, gesto.DeltaPosition.y, 0);
        }
    }

}
