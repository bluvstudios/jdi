﻿using System.Collections;
using System.Collections.Generic;
using TouchScript.Gestures;
using UnityEngine;

public class PanelOpener  : MonoBehaviour{
    
    public GameObject Panel, buddyCollision;

    public void OpenPanel()
    {
        if (Panel != null)
        {
            buddyCollision.GetComponent<BoxCollider2D>().enabled = false;
            Panel.SetActive(true);
            gameObject.SetActive(false);
        }
    }
}
