﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Loja : MonoBehaviour {

    private bool controleAbas, funcBot;
    public GameObject[] abas;
    void Start () {

    }

    void Update () {
        if (funcBot) {

            if (controleAbas) {
                abas[0].SetActive (true);
                abas[1].SetActive (false);

            } else {
                abas[0].SetActive (false);
                abas[1].SetActive (true);
            }

            funcBot = false;
        }

    }

    public void abasFunc () {
        funcBot = true;
        if (controleAbas) { controleAbas = false; } else if (!controleAbas) { controleAbas = true; }
    }

    bool estadoLoja;

    public GameObject loja;
    public void lojaAba () {

        if (estadoLoja) estadoLoja = false;
        else estadoLoja = true;

        if (estadoLoja) {
            Destroy (GameObject.Find ("Loja 1(Clone)"));
        }

    }
}