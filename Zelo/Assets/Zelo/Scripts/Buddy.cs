﻿using System.Collections;
using System.Collections.Generic;
using TouchScript.Gestures;
using UnityEngine;
using UnityEngine.Playables;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class Buddy : MonoBehaviour {
    TapGesture Tap;

    //public PlayableDirector bump;    COLOCAR ANIMAÇÃO ASSIM QUE TUDO ESTIVER FUNCIONANDO, NO OnEnable DO OBJETO DE PREFERENCIA
    // static public bool ifsettings;

    private GameObject destroy;

    public GameObject cozinhaPop;
    public GameObject quartoPop;
    public GameObject academiaPop;
    public GameObject setas;
    public GameObject lojaComida;
    public GameObject config;
    public GameObject loja;

    public static bool popOn; //estado do Pop Up

    void Awake () {
        Tap = GetComponent<TapGesture> ();
    }

    private void OnEnable () {
        Tap.Tapped += Processa;
    }

    private void OnDisable () {
        Tap.Tapped -= Processa;
    }

    private void Processa (object sender, System.EventArgs e) {

        if (SwipeBackground.estado == "cozinha") {
            if (!popOn) {
                Instantiate (cozinhaPop, transform.position, Quaternion.identity);
                popOn = true;
                setas.SetActive (false);
                config.SetActive (false);

            } else if (popOn) {
                Destroy (GameObject.Find (cozinhaPop.gameObject.name + "(Clone)"));
                popOn = false;
                setas.SetActive (true);
                config.SetActive (true);
            }
        }

        if (SwipeBackground.estado == "quarto") {
            if (!popOn) {
                Instantiate (quartoPop, transform.position, Quaternion.identity);
                popOn = true;
                setas.SetActive (false);
                config.SetActive (false);
            } else if (popOn) {
                Destroy (GameObject.Find (quartoPop.gameObject.name + "(Clone)"));
                popOn = false;
                setas.SetActive (true);
                config.SetActive (true);
            }
        }

        if (SwipeBackground.estado == "academia") {
            if (!popOn) {
                Instantiate (academiaPop, transform.position, Quaternion.identity);
                popOn = true;
                setas.SetActive (false);
                config.SetActive (false);
            } else if (popOn) {
                Destroy (GameObject.Find (academiaPop.gameObject.name + "(Clone)"));
                popOn = false;
                setas.SetActive (true);
                config.SetActive (true);
            }
        }
    }
    public void muteSound () {
        AudioListener.pause = !AudioListener.pause;
    }
    #region botoesQuarto

    public void Dormir () { }
    public void Banho () { }
    public void MiniGameQuarto () {
        SceneManager.LoadScene ("MiniGameQuarto", LoadSceneMode.Single);
    }
    public void TrocaDeRoupa () { }

    bool estadoLoja;
    public void lojaAba () {

        if (estadoLoja) estadoLoja = false;
        else estadoLoja = true;

        Instantiate (loja);

    }

    #endregion

    #region botoesCozinha

    public void Comer () {
        Instantiate (lojaComida);
        Destroy (GameObject.Find (cozinhaPop.gameObject.name + "(Clone)"));
        popOn = false;
    }
    public void Medicar () {
        GameObject.Find ("Background").GetComponent<Barras> ().ShotInsulina ();
    }
    public void MiniGameCozinha () {
        SceneManager.LoadScene ("MiniGame Cozinha", LoadSceneMode.Single);
    }

    #endregion

    #region botoesAcademia

    public void Exercicio () { }
    public void Caminhar () {
        SceneManager.LoadScene ("GPS");
    }
    public void MiniGameAcademia () { }

    #endregion

}