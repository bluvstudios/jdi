﻿using System.Collections;
using System.Collections.Generic;
using TouchScript.Gestures;
using TouchScript.Gestures.TransformGestures;
using UnityEngine;
using UnityEngine.EventSystems;

public class SwipeBackground : MonoBehaviour {

    public GameObject background;

    const float COZINHA = 6f;
    const float QUARTO = 0;
    const float ACADEMIA = -6f;
    public static string estado = "quarto";
    public float easing = 0.5f;
    bool esquerdaEstado, direitaEstado;
    public GameObject esquerdaBot, direitaBot;

    private void Start() {
        estado = "quarto";
    }

    void Update () {
        if (esquerdaEstado) {
            if (estado == "quarto") {
                StartCoroutine (SmoothMove (background.transform.position, COZINHA, easing));
                esquerdaBot.SetActive(false);
                estado = "cozinha";
                esquerdaEstado = false;
            }

            if (estado == "academia") {
                StartCoroutine (SmoothMove (background.transform.position, QUARTO, easing));
                direitaBot.SetActive(true);
                estado = "quarto";
                esquerdaEstado = false;
            }
        }

        if (direitaEstado) {
            if (estado == "quarto") {
                StartCoroutine (SmoothMove (background.transform.position, ACADEMIA, easing));
                direitaBot.SetActive(false);
                estado = "academia";
                direitaEstado = false;
            }
            if (estado == "cozinha") {
                StartCoroutine (SmoothMove (background.transform.position, QUARTO, easing));
                esquerdaBot.SetActive(true);
                estado = "quarto";
                direitaEstado = false;
            }
        }
    }
    IEnumerator SmoothMove (Vector3 startpos, float endpos, float seconds) {
        float t = 0f;
        while (t <= 1.0) {
            t += Time.deltaTime / seconds;
            background.transform.position = Vector3.Lerp (startpos, new Vector3 (endpos, 0, background.transform.position.z), Mathf.SmoothStep (0f, 1f, t));
            yield return null;
        }
    }

    public void esquerda () {
        esquerdaEstado = true;
    }

    public void direita () {
        direitaEstado = true;
    }

}