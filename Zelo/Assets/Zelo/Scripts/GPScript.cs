﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GPScript : MonoBehaviour
{
    public float oldlatitude, oldlongitude, newlatitude, newlongitude;
    bool first;
    double dTotal;
    public Text gpsdisplay, status, lat, lon;

    // Start is called before the first frame update
    void Start()
    {
        first = false;
        StartCoroutine(StartLocationService());
    }

    private IEnumerator StartLocationService()
    {
        if (!Input.location.isEnabledByUser)
        {
            Debug.Log("Usuário não permitiu acesso ao GPS");
            status.text = ("Usuário não permitiu acesso ao GPS");
            yield break;
        }

        Input.location.Start();
        int maxWait = 15;
        while(Input.location.status == LocationServiceStatus.Initializing && maxWait > 0)
        {
            yield return new WaitForSeconds(1);
            maxWait--;
        }

        if(maxWait < 0)
        {
            Debug.Log("Tempo de Espera Expirado");
            status.text = ("Tempo de Espera Expirado");
            yield break;
        }
        if(Input.location.status == LocationServiceStatus.Failed)
        {
            Debug.Log("Falha");
            status.text = ("Falha");
            yield break;
        }

        newlatitude = Input.location.lastData.latitude;
        newlongitude = Input.location.lastData.longitude;
        lat.text = ("Latitude: " + newlatitude);
        lon.text = ("Longitude: " + newlongitude);

        if (first == false)
        {
            first = true;
        }
        else
        {
            var R = 6371000; // metres
            var o1 = oldlatitude * Mathf.Deg2Rad;
            var o2 = newlatitude * Mathf.Deg2Rad;
            var yo = (newlatitude - oldlatitude) * Mathf.Deg2Rad;
            var yd = (newlongitude - oldlongitude) * Mathf.Deg2Rad;

            var a = Mathf.Sin(yo / 2) * Mathf.Sin(yo / 2) +
                    Mathf.Cos(o1) * Mathf.Cos(o2) *
                    Mathf.Sin(yd / 2) * Mathf.Sin(yd / 2);
            var c = 2 * Mathf.Atan2(Mathf.Sqrt(a), Mathf.Sqrt(1 - a));

            var d = R * c;
            
            dTotal += d;
            gpsdisplay.text = ("Distância Andada: " + d + System.Environment.NewLine + "Distância Total: " + dTotal);
        }

        oldlatitude = newlatitude;
        oldlongitude = newlongitude;

        StartCoroutine(StartLocationService());

        yield break;
    }

    public void VoltarGPS()
    {
        SceneManager.LoadScene("Zelo", LoadSceneMode.Single);
    }
}
