﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FiltroDeItens : MonoBehaviour
{
    public int scoreMGC;
    public int moedashere;
    public Text textmoedas, textscore;

    // Start is called before the first frame update
    void Start()
    {
        Static.moedas = PlayerPrefs.GetInt("Moedas", 0);
        Static.highscore = PlayerPrefs.GetInt("Highscore", 0);

        moedashere = 0;
        scoreMGC = 0;
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.gameObject.tag == "Comida")
        {
            scoreMGC += 4;
            Static.moedas += 1;
            moedashere += 1;
        }
        else if (collision.gameObject.tag == "Comida1")
        {
            scoreMGC += 6;
            Static.moedas += 2;
            moedashere += 2;
        }
        else if (collision.gameObject.tag == "Comida2")
        {
            scoreMGC += 8;
            Static.moedas += 3;
            moedashere += 3;
        }

        textmoedas.text = "Moedas: " + moedashere;
        textscore.text = "Pontuação: " + scoreMGC;
        Destroy(collision.gameObject);
    }
}
