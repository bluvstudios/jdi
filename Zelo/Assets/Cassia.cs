﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Cassia : MonoBehaviour
{
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.gameObject.tag == "Comida" || collision.gameObject.tag == "Comida1" || collision.gameObject.tag == "Comida2")
        {
            GameObject.Find("Canvas").GetComponent<MGManager>().HeartAtualize();
            Destroy(collision.gameObject);
        }
    }
}