﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MGManager : MonoBehaviour
{
    public GameObject pf1, pf2, pf3, heart1, heart2, heart3;
    float timer, maxtime;
    int comidanum = 3;
    int hp = 3;

    // Start is called before the first frame update
    void Start()
    {
        maxtime = 3;
    }

    // Update is called once per frame
    void Update()
    {
        Spawner();
    }

    void Spawner()
    {
        if (timer < maxtime)
        {
            timer += Time.deltaTime;
        }
        else
        {
            Spawn();
            if (maxtime > 1) maxtime -= 0.1f; 
            timer = 0;
        }
    }

    void Spawn()
    {
        float random = Random.value;

        if(random < 0.3333f)
        {
            Instantiate(pf1, new Vector3(Random.Range(-2.5f, 2.3f), 5.5f, 0), Quaternion.identity);
        }
        else if (random < 0.6666f)
        {
            Instantiate(pf2, new Vector3(Random.Range(-2.5f, 2.3f), 5.5f, 0), Quaternion.identity);
        }
        else if (random < 1)
        {
            Instantiate(pf3, new Vector3(Random.Range(-2.5f, 2.3f), 5.5f, 0), Quaternion.identity);
        }
    }

    public void HeartAtualize()
    {
        if(hp == 3)
        {
            heart3.SetActive(false);
        }
        else if(hp == 2)
        {
            heart2.SetActive(false);
        }

        hp -= 1;
        if(hp <= 0)
        {
            Static.moedashere = GameObject.Find("Filtro").GetComponent<FiltroDeItens>().moedashere;
            Static.score = GameObject.Find("Filtro").GetComponent<FiltroDeItens>().scoreMGC;
            if(Static.score > Static.highscore)
            {
                Static.highscore = Static.score;
            }

            PlayerPrefs.SetInt("Moedas", Static.moedas);
            PlayerPrefs.SetInt("Highscore", Static.highscore);

            SceneManager.LoadScene("FimCozinha");
        }
    }
}
