﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Carrinho : MonoBehaviour
{
    Vector3 touchPos;

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        if(Input.touchCount > 0)
        {
            Touch toque = Input.GetTouch(0);
            touchPos = Camera.main.ScreenToWorldPoint(toque.position);
            gameObject.transform.position = new Vector3(touchPos.x, gameObject.transform.position.y, gameObject.transform.position.z);
        }

        if(gameObject.transform.position.x < -1.91f)
        {
            gameObject.transform.position = new Vector3(-1.91f, gameObject.transform.position.y, gameObject.transform.position.z);
        }
        else if(gameObject.transform.position.x > 2f)
        {
            gameObject.transform.position = new Vector3(2f, gameObject.transform.position.y, gameObject.transform.position.z);
        }
    }
}
